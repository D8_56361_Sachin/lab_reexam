<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sf"  uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<sf:form modelAttribute="command" method="post" action="/insert">
		Id
		<sf:input path="id" readonly="true"/> <br>
		
		Name
		<sf:input path="name"/> <br>
		
		Duration
		<sf:input path="duration"/> <br>
		
		Course
		<sf:input path="course"/> <br>
		
		Teacher id
		<sf:input path="tid"/> <br>
		
		<input type="submit" value="add">
	</sf:form>


</body>
</html>