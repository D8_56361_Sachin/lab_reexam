<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<table>
		<thead>
			<tr>
				<td>Name</td>
				<td>Email</td>
				<td>DOB</td>
				<td>Role</td>
				<td>Action</td> 
			</tr>
		</thead>
		<tr>
		
		<tbody>

			<tr>
				<c:forEach var="t" items="${teachers}">
					<td>${t.name}</td> 
					<td>${t.email}</td>
					<td>${t.dob}</td>
					<td>${t.role}</td>
					<td> <a href="/delete?id=${t.id}"> delete </a> </td><br>
				</c:forEach>
			</tr>
		</tbody>
	</table>



</body>
</html>